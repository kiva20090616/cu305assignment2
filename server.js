var restify = require('restify');
var request = require('request');
var Router = require('restify-router').Router;
var routerInstance = new  Router();
var nano = require('nano')('http://localhost:5984');


 

var weatherlist = nano.use('weatherlist');
 


var server = restify.createServer();
server.use(restify.bodyParser());
server.use(restify.authorizationParser());
server.use(restify.acceptParser(server.acceptable));  
server.use(restify.authorizationParser());  
server.use(restify.CORS());                 
server.use(restify.dateParser());           
server.use(restify.queryParser());          
server.use(restify.gzipResponse());         
server.use(restify.requestLogger());        
server.use(restify.conditionalRequest());   
server.use(restify.fullResponse());        
server.use(restify.jsonBodyParser());
server.post('/test', function(req, res, next){
    console.log(req.body);
});


var port = process.env.PORT || 3000;
server.listen(port, function() {
  console.log('%s listening at %s', server.name, server.url);
});




routerInstance.applyRoutes(server);

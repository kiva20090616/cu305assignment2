

/* Controllers */
  var app = angular.module('myApp', []);
 //showAll controller         
app.controller('myCtrl', function($scope, $http) {
    
    $http.get("/showAll")
    .then(function(response) {
       
       
        console.log(response.data.result);
       
        $scope.obj  = response.data.result;
    }, function myError(response) {
        console.log(response.statusText);
    });
});

 //search controller  
app.controller('searchCtrl', function($scope, $http) {
     $scope.submitsearForm = function() {
    $http.get("/searchweather/"+$scope.searchText)
    .then(function(response) {
       
        $scope.result = response.data;
        console.log(response.data);
       
       
    }, function myError(response) {
        console.log(response.statusText);
    });
         
     }
});

app.controller('editCtrl', function($scope, $http) {
     $scope.submitsearForm = function() {
       $http.get("/searchweather/"+$scope.searchText)
    .then(function(response) {
       
        $scope.editresult = response.data;
        sessionStorage.setItem("id", $scope.editresult._id);
        sessionStorage.setItem("rev", $scope.editresult._rev);
        
        //var _id = $scope.editresult._id;
          //var _rev = $scope.editresult._rev;
       
       
    }, function myError(response) {
        console.log(response.statusText);
    });
         }
     $scope.editCtrlForm = function() {
         
            var data = $scope.article;
           var update = angular.toJson(data);
            var updatedata = JSON.parse(update);
           console.log(updatedata.locat);
          var data ={
               "_id":sessionStorage.getItem("id"),
               "_rev":sessionStorage.getItem("rev"),
               "title":updatedata.title,
               "author":updatedata.author,
               "desc": updatedata.desc,
               "location": updatedata.locat,
          }
          
    
var request = $http({
                    method: "put",
                    url: "/modifyweather",
                    data: data
                });
    
request.success(function(data, status, headers, config) {
				alert( "message:  Updating sucessfully with " + JSON.stringify({data: data}));
		});
		request.error(function(data, status, headers, config) {
			alert( "failure message: " + JSON.stringify({data: data}));
		});	
       }
       });



app.controller('delCtrl', function($scope, $http) {
      $scope.submitdelForm = function() {
    var data = $scope.itemname;
    var r = confirm("Warning message: Are you sure to delete" + data+"?");
    if (r == true) {
      
 $http.delete("/delweather/"+data)
    .then(function(response) {
       	alert( "message: "+data+"Deleted sucessfully !");
        $scope.obj  = response.data.result;
    }, function myError(response) {
         	alert( "message: "+data+" doesn't existed !");
        console.log(response.statusText);
    });
      
        
    } else {
       
        
    }
     
 }
    
 
});

app.controller('createWeather', ['$scope','$http',function($scope, $http) {
    $scope.data = {};
    
      // calling our submit function.
      
       $scope.submitMyForm = function() {
           var data = $scope.article;
           var jsondata = angular.toJson(data);
         console.log(jsondata);

var request = $http({
                    method: "post",
                    url: "/createweather",
                    data: jsondata
                });
    
request.success(function(data, status, headers, config) {
				alert( "message: Creating sucessfully with " + JSON.stringify({data: data}));
		});
		request.error(function(data, status, headers, config) {
			alert( "failure message: " + JSON.stringify({data: data}));
		});	
       }
        $scope.reset = function() {
        $scope.article = angular.copy($scope.data);
      };

      $scope.reset();
     

   }]);
   
   
   
   app.controller('createUser', ['$scope','$http',function($scope, $http) {
    $scope.data = {};
    
      // calling our submit function.
      
       $scope.submitForm = function() {
           var data = $scope.user;
           var jsondata = angular.toJson(data);
         console.log(jsondata);

var request = $http({
                    method: "post",
                    url: "/createUser",
                    data: jsondata
                });
    
request.success(function(data, status, headers, config) {
				alert( "message: Creating sucessfully with " + JSON.stringify({data: data}));
		});
		request.error(function(data, status, headers, config) {
			alert( "failure message: " + JSON.stringify({data: data}));
		});	
       }
        $scope.reset = function() {
        $scope.article = angular.copy($scope.data);
      };

      $scope.reset();
     

   }]);
      app.controller('loginCtrl', function($scope, $http) {
       $scope.submitForm = function() {
           var data = $scope.user.id;
          
            $http.get("/searchUser/"+data)
    .then(function(response) {
       	alert("login successful, welcome "+data);
        window.location.reload();
        console.log(response.data.result);
        $scope.session = data;
        sessionStorage.setItem("username",$scope.session);
           var i= sessionStorage.getItem("username");
           console.log(i);
       
     
        
    }, function myError(response) {
        	alert("login unsuccessfully,Please try again!");
        console.log(response.statusText);
    });
         console.log(data);
           
       }  });
         /*
            $scope.Save = function () {
                $localStorage.LocalMessage = "LocalStorage: My name is Mudassar Khan.";
                $sessionStorage.SessionMessage = "SessionStorage: My name is Mudassar Khan.";
            }
            $scope.Get = function () {
                $window.alert($localStorage.LocalMessage + "\n" + $sessionStorage.SessionMessage);
            }
              $http.get("/searchUser/"+)
    .then(function(response) {
       
       
        console.log(response.data.result);
       
        $scope.obj  = response.data.result;
    }, function myError(response) {
        console.log(response.statusText);
    });*/
            
            
      